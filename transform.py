import datetime

import pyspark.sql.functions as F

# from spotify_api import get_artists_by_ids, get_sp
# from utils import init_spark

import os

from pyspark.sql import SparkSession

# os.environ["PYSPARK_PYTHON"] = "/usr/bin/python3.6"
# os.environ["PYSPARK_DRIVER_PYTHON"] = "/usr/bin/python3.6"

# os.environ["HADOOP_CONF_DIR"] = "/etc/hadoop/conf.cloudera.hdfs"
# os.environ["YARN_CONF_DIR"] = "/etc/hadoop/conf.cloudera.yarn"
# os.environ["HIVE_CONF_DIR"] = "/etc/hive/conf.cloudera.hive"


def init_spark(job_name):
    # spark = SparkSession.builder.master("yarn").appName(job_name).getOrCreate()
    spark = SparkSession.builder.appName(job_name).getOrCreate()
    spark.conf.set("spark.sql.sources.partitionOverwriteMode", "dynamic")
    return spark


def get_data(spark, date):
    df = spark.read.parquet("data_raw")
    return df.filter(F.col("date") == date)


def transform_data(date):
    spark = init_spark("transform_{}".format(date))
    df = get_data(spark, date)

    df = df.groupBy("id", "date").agg(
        F.sum(F.col("popularity")),
        F.count(F.col("popularity")),
        F.avg(F.col("popularity")),
        F.expr("percentile_approx(popularity, 0.5)"),
    )

    l_id = df.select(F.col("id")).collect()
    l_id = [row.id for row in l_id]

    # sp = get_sp()

    # l_artists = get_artists_by_ids(sp, l_id)

    # artist_data = [
    #     (id_, a["genres"], a["followers"]["total"], a["name"])
    #     for id_, a in zip(l_id, l_artists)
    # ]

    # df_2 = spark.createDataFrame(artist_data, ["id", "genres", "followers", "name"])

    # df_final = df.join(df_2, on=["id"], how="inner")

    header = [
        "id",
        "date",
        "sum_pop",
        "count_pop",
        "avg_pop",
        "median_pop",
        # "genres",
        "followers",
        "name",
    ]

    for i, column in enumerate(df.columns):
        df = df.withColumnRenamed(column, header[i])

    df.write.mode("overwrite").partitionBy("date").parquet("data_processed")


if __name__ == "__main__":
    # for i in range(1, 13):
    #     print("month {}".format(i))
    #     date = datetime.date(2020, i, 1)
    #     transform_data(date)
    date = datetime.date(2020, 12, 10)
    transform_data(date)