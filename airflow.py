import datetime
from dateutil.relativedelta import relativedelta

from generate_output import read
from import_sources import main
from transform import transform_data

if __name__ == "__main__":
    for i in range(1, 13):
        print("month {}".format(i))
        date = datetime.date(2020, i, 1)
        main(date)
        transform_data(date)
        read(date - relativedelta(months=12), date)