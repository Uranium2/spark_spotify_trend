import asyncio
import datetime

import spotipy
from spotipy.client import SpotifyException
from spotipy.oauth2 import SpotifyClientCredentials


def get_sp():
    return spotipy.Spotify(
        auth_manager=SpotifyClientCredentials(
            client_id="23c9d79d05d44f36b6b852683b21a2d9",
            client_secret="05256b7cb96f41b9a82bd104bf59a402",
            requests_timeout=3600,
        ),
    )


def get_all_items(sp, items_page):
    while True:
        if "items" in items_page:
            for item in items_page["items"]:
                yield item
        else:
            key_self_name = next(iter(items_page))
            for item in items_page[key_self_name]["items"]:
                yield item
            items_page = items_page[key_self_name]

        if not items_page["next"]:
            return

        items_page = sp.next(items_page)


def get_trending_tracks(sp, categories):
    for c in get_all_items(sp, categories):
        try:
            playlists = sp.category_playlists(c["id"])
        except (SpotifyException, TypeError):
            continue

        for p in get_all_items(sp, playlists):
            try:
                tracks = sp.playlist_tracks(p["id"])
            except (SpotifyException, TypeError):
                continue

            for t in get_all_items(sp, tracks):
                if t["track"]:
                    yield t["track"]


def get_tracks(sp, playlists_json):
    for item in playlists_json["playlists"]["items"]:
        try:
            tracks = sp.playlist(item["id"], fields="tracks,next")["tracks"]
        except (SpotifyException, TypeError):
            continue
        for i in range(len(tracks["items"])):
            yield tracks["items"][i]["track"]


def get_trending_artists(sp, date, n=500):
    featured_playlists = get_featured_playlists(sp, date)

    seen_artists = set()
    artists = []

    for track in get_tracks(sp, featured_playlists):
        for artist in track["artists"]:
            artists.append({"id": artist["id"], "popularity": track["popularity"]})
            seen_artists.add(artist["id"])

            if len(seen_artists) >= n:
                return artists

    return artists


def get_featured_playlists(sp, date):
    date = date.strftime("%Y-%m-%dT%H:%M:%S.%f%z")
    return sp.featured_playlists(timestamp=date)


def get_artists_by_ids(sp, ids):
    async def query_artists(sp, ids):
        return sp.artists(ids)["artists"]

    chunk_size = 50
    artists_chunks = (ids[i : i + chunk_size] for i in range(0, len(ids), chunk_size))
    promises = asyncio.gather(*[query_artists(sp, chunk) for chunk in artists_chunks])

    results = asyncio.get_event_loop().run_until_complete(
        promises
    )  # F*ck you deprecated python3.6
    # results = asyncio.run(promises)  # >=python3.7  \e/

    arts_flatten = []
    for arts in results:
        arts_flatten.extend(arts)
    return arts_flatten


if __name__ == "__main__":
    # date = datetime.datetime.now().isoformat()
    date = datetime.date(2020, 12, 9)
    sp = get_sp()
    res = get_trending_artists(sp, date)

    for x in res:
        print(x)
