import os

from pyspark.sql import SparkSession

os.environ["PYSPARK_PYTHON"] = "/usr/bin/python3.6"
os.environ["PYSPARK_DRIVER_PYTHON"] = "/usr/bin/python3.6"

os.environ["HADOOP_CONF_DIR"] = "/etc/hadoop/conf.cloudera.hdfs"
os.environ["YARN_CONF_DIR"] = "/etc/hadoop/conf.cloudera.yarn"
os.environ["HIVE_CONF_DIR"] = "/etc/hive/conf.cloudera.hive"


def init_spark(job_name):
    spark = SparkSession.builder.master("yarn").appName(job_name).getOrCreate()
    spark.conf.set("spark.sql.sources.partitionOverwriteMode", "dynamic")
    return spark
