import datetime

from pyspark.sql import functions as F
from pyspark.sql.types import IntegerType 

import os

from pyspark.sql import SparkSession

# os.environ["PYSPARK_PYTHON"] = "/usr/bin/python3.6"
# os.environ["PYSPARK_DRIVER_PYTHON"] = "/usr/bin/python3.6"

# os.environ["HADOOP_CONF_DIR"] = "/etc/hadoop/conf.cloudera.hdfs"
# os.environ["YARN_CONF_DIR"] = "/etc/hadoop/conf.cloudera.yarn"
# os.environ["HIVE_CONF_DIR"] = "/etc/hive/conf.cloudera.hive"


def init_spark(job_name):
    # spark = SparkSession.builder.master("yarn").appName(job_name).getOrCreate()
    spark = SparkSession.builder.appName(job_name).getOrCreate()
    spark.conf.set("spark.sql.sources.partitionOverwriteMode", "dynamic")
    return spark



def read(date_1, date_2):
    assert date_2 > date_1, "date_2 must be greater than date_1"

    spark = init_spark("generate_output_{}_{}".format(date_1, date_2))
    df = spark.read.parquet("data_processed")
    df = df.filter(F.col("date").between(date_1, date_2))

    df = df.groupBy("id", "name", "genres").agg(*[F.collect_list(x).alias(x) for x in ["sum_pop", "count_pop", "avg_pop" , "followers", "date"]])

    max_index = F.udf(lambda x: x.index(max(x)), IntegerType())
    min_index = F.udf(lambda x: x.index(min(x)), IntegerType())

    df = df.withColumn("argmax", max_index(F.col("date")))
    df = df.withColumn("argmin", min_index(F.col("date")))

    for col in ["sum_pop", "count_pop", "avg_pop" , "followers"]:
        original = F.col(col).getItem(F.col("argmin"))
        new = F.col(col).getItem(F.col("argmax"))
        df = df.withColumn("variation_{}".format(col), F.round((new - original) / original * 100, 2))

    df = df.drop("argmax", "argmin")
    df.coalesce(1).write.mode("overwrite").json("outputs_raw/result_{}_{}.json".format(date_1, date_2))


if __name__ == "__main__":
    date_1 = datetime.date(2020, 12, 9)
    date_2 = datetime.date(2020, 12, 10)

    read(date_1, date_2)
